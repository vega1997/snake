package cat.escolapia.damviod.pmdm.snake;

import java.util.Random;

public class World {
    static final int WORLD_WIDTH = 10;
    static final int WORLD_HEIGHT = 13;
    static final int SCORE_INCREMENT = 10;
    static final float TICK_INITIAL = 0.5f;
    static final float TICK_DECREMENT = 0.02f;

    public Snake snake;
    public Diamond diamond;
    public Diamond diamond2;
    public Wall wall;
    public boolean gameOver = false;;
    public int score = 0;
    public int bg_score = 0;

    boolean fields[][] = new boolean[WORLD_WIDTH][WORLD_HEIGHT];
    Random random = new Random();
    float tickTime = 0;
    float tick = TICK_INITIAL;

    public World() {
        snake = new Snake();
       placeDiamond(1);
        placeDiamond(2);
        placeWall();
    }

    private void placeDiamond(int n) {
            for (int x = 0; x < WORLD_WIDTH; x++) {
                for (int y = 0; y < WORLD_HEIGHT; y++) {
                    fields[x][y] = false;
                }
            }

            int len = snake.parts.size();
            for (int i = 0; i < len; i++) {
                SnakePart part = snake.parts.get(i);
                fields[part.x][part.y] = true;
            }

            int diamondX = random.nextInt(WORLD_WIDTH);
            int diamondY = random.nextInt(WORLD_HEIGHT);
            while (true) {
                if (fields[diamondX][diamondY] == false) break;
                diamondX += 1;
                if (diamondX >= WORLD_WIDTH) {
                    diamondX = 0;
                    diamondY += 1;
                    if (diamondY >= WORLD_HEIGHT) {
                        diamondY = 0;
                    }
                }
            }
        if(n==1){
            diamond = new Diamond(diamondX, diamondY, Diamond.TYPE_1);
        }else{
            diamond2 = new Diamond(diamondX, diamondY, Diamond.TYPE_1);
        }


    }

    private void placeWall() {

        int wallX = random.nextInt(WORLD_WIDTH);
        int wallY = random.nextInt(WORLD_HEIGHT);
        while (true) {
            if (fields[wallX][wallY] == false) break;
            wallX += 1;
            if (wallX >= WORLD_WIDTH) {
                wallX = 0;
                wallY += 1;
                if (wallY >= WORLD_HEIGHT) {
                    wallY = 0;
                }
            }
        }
        wall = new Wall(wallX, wallY);

    }

    public void update(float deltaTime) {
        if (gameOver) return;

        tickTime += deltaTime;
        while (tickTime > tick) {
            tickTime -= tick;
            snake.advance();
            if (snake.checkXoca()) {
                gameOver = true;
                return;
            }
            SnakePart head = snake.parts.get(0);
            if(head.x == diamond.x && head.y == diamond.y){
                score = score + SCORE_INCREMENT;
                bg_score = bg_score + SCORE_INCREMENT;
                placeDiamond(1);
                placeWall();
                snake.allarga();
                tick = tick - TICK_DECREMENT;

            }
            else  if(head.x == diamond2.x && head.y == diamond2.y){
                score = score + SCORE_INCREMENT;
                bg_score = bg_score + SCORE_INCREMENT;
                placeDiamond(2);
                placeWall();
                snake.allarga();
                tick = tick - TICK_DECREMENT;

            }
            else if(head.x == wall.x && head.y == wall.y){
                gameOver = true;
            }
        }
    }
}
